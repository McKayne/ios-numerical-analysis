//
//  Jacobi.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 08.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Jacobi {
    
    private var a: [[Double]], n: Int
    
    private func maxAbsNondiag() -> (Double, Int, Int) {
        var max: Double = 0
        var maxI: Int = 0, maxJ: Int = 0
        
        for i in 0..<a.count {
            for j in 0..<a.count {
                if i != j {
                    if (abs(a[i][j]) > abs(max)) {
                        max = a[i][j]
                        maxI = i
                        maxJ = j
                    }
                }
            }
        }
        
        return (max, maxI, maxJ)
    }
    
    private func phi(max: (Double, Int, Int)) -> Double {
        if a[max.1][max.1] != a[max.2][max.2] {
            return 1 / 2 * atan(2 * a[max.1][max.2] / (a[max.1][max.1] - a[max.2][max.2]))
        } else {
            return 3.14 / 4
        }
    }
    
    private func rotationMatrix(max: (Double, Int, Int)) -> [[Double]] {
        var u: [[Double]] = []
        
        for i in 0..<a.count {
            var uRow: [Double] = []
            for j in 0..<a.count {
                if i == j {
                    uRow.append(1)
                } else {
                    uRow.append(0)
                }
            }
            u.append(uRow)
        }
        
        u[max.1][max.1] = cos(phi(max: max))
        u[max.1][max.2] = -sin(phi(max: max))
        u[max.2][max.1] = sin(phi(max: max))
        u[max.2][max.2] = cos(phi(max: max))
        
        return u
    }
    
    private func transpose(mat: [[Double]]) -> [[Double]] {
        var tr: [[Double]] = []
        
        for i in 0..<mat.count {
            var trRow: [Double] = []
            for j in 0..<mat.count {
                trRow.append(mat[j][i])
            }
            tr.append(trRow)
        }
        
        return tr
    }
    
    internal func matrixMultiply(a: [[Double]], b: [[Double]]) -> [[Double]] {
        var c: [[Double]] = []
        
        for i in 0..<a.count {
            var cRow: [Double] = []
            for j in 0..<a.count {
                var sum: Double = 0
                for r in 0..<a.count {
                    sum += a[i][r] * b[r][j]
                }
                cRow.append(sum)
            }
            c.append(cRow)
        }
        
        return c
    }
    
    func findSolution() -> (lambda: [Double], x: [[Double]]) {
        
        var x: [[Double]] = []
        
        for _ in 0...10 {
            let maxAbs = maxAbsNondiag()
            //print("Max \(maxAbs.0), \(maxAbs.1), \(maxAbs.2)")
            
            let u = rotationMatrix(max: maxAbs)
            //printMatrix(mat: u)
            
            //print("A = ")
            a = matrixMultiply(a: matrixMultiply(a: transpose(mat: u), b: a), b: u)
            //printMatrix(mat: a)
            
            if x.count == 0 {
                x = u
            } else {
                x = matrixMultiply(a: x, b: u)
            }
        }
        
        var lambda: [Double] = []
        for i in 0..<a.count {
            lambda.append(a[i][i])
        }
        
        
        
        return (lambda, x)
    }

    init(a: [[Double]]) {
        self.a = a
        n = a.count
    }
}
