//
//  Seidel.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 08.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Seidel {
    
    private var a: [[Double]], b: [Double], n: Int
    
    private func euclideanNorm(x: [Double]) -> Double {
        var norm: Double = 0
        
        for i in 0..<x.count {
            norm += pow(x[i], 2)
        }
        
        return sqrt(norm)
    }
    
    private func matrixNorm(a: [[Double]]) -> Double {
        var norm: Double = 0
        
        for i in 0..<a.count {
            var sum: Double = 0
            
            for j in 0..<a.count {
                sum += abs(a[i][j])
            }
            
            if sum > norm {
                norm = sum
            }
        }
        
        return norm
    }
    
    private func eps(a: [[Double]], x: [Double]) -> Double {
        return matrixNorm(a: a) / (1 - matrixNorm(a: a)) * euclideanNorm(x: x)
    }
    
    private func zeidel(a: [[Double]], b: [Double], x: [Double]) -> [Double] {
        var alpha: [[Double]] = []
        var beta: [Double] = []
        for i in 0..<a.count {
            var alphaRow: [Double] = []
            for j in 0..<a.count {
                if i != j {
                    alphaRow.append(-a[i][j] / a[i][i])
                } else {
                    alphaRow.append(0)
                }
            }
            alpha.append(alphaRow)
            beta.append(b[i] / a[i][i])
        }
        
        var nextX: [Double] = []
        for i in 0..<alpha.count {
            var xValue: Double = beta[i]
            for j in 0..<alpha.count {
                
                if j < i {
                    //nextX.append(beta[i] + alpha[i][j] * nextX[j])
                    xValue += alpha[i][j] * nextX[j]
                } else {
                    //nextX.append(beta[i] + alpha[i][j] * x[j])
                    xValue += alpha[i][j] * x[j]
                }
                
            }
            nextX.append(xValue)
        }
        
        return nextX
    }
    
    func findSolution() -> [Double] {
        var x: [Double] = []
        
        var beta: [Double] = []
        
        for i in 0..<a.count {
            beta.append(b[i] / a[i][i])
            x.append(beta[i])
        }
        
        var normX: [Double] = []
        var iter: UInt8 = 1
        repeat {
            var nextX: [Double] = zeidel(a: a, b: b, x: x)
            
            normX = []
            for i in 0..<a.count {
                normX.append(nextX[i] - x[i])
            }
            
            x = nextX
            
            print("Iteration \(iter)")
            iter += 1
        } while eps(a: a, x: normX) > pow(10.0, -16)
        
        return x
    }
    
    init(a: [[Double]], b: [Double]) {
        self.a = a
        self.b = b
        n = a.count
    }
}
