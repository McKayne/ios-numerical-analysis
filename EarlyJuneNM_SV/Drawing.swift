//
//  Drawing.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 09.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation
import UIKit

class Drawing: UIView {
    
    private var f: (Double) -> Double, startFrom: Double, endAt: Double, step: Double
    
    init(frame: CGRect, f: @escaping (Double) -> Double, startFrom: Double, endAt: Double, step: Double) {
        self.f = f
        self.startFrom = startFrom
        self.endAt = endAt
        self.step = step
        
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func toLocalCoords(x: Double, y: Double, h: CGFloat, w: CGFloat) -> (localX: CGFloat, localY: CGFloat) {
        
        let percent = (endAt - startFrom) / 100.0
        let multiplier = (x - startFrom) / percent / 100.0
        
        return (w * CGFloat(multiplier), h * 0.5 - CGFloat(y) * h * 0.5)
    }
    
    private func fillRect(h: CGFloat, w: CGFloat) {
        let color:UIColor = UIColor.white
        
        let drect = CGRect(x: 0.0, y: 0.0, width: w, height: h)
        let bpath:UIBezierPath = UIBezierPath(rect: drect)
        
        color.set()
        bpath.stroke()
        
        bpath.fill()
    }
    
    private func drawLine(from: (x: Double, y: Double), to: (x: Double, y: Double), color: UIColor) {
        let aPath = UIBezierPath()
        
        aPath.move(to: CGPoint(x: from.x, y: from.y))
        
        aPath.addLine(to: CGPoint(x: to.x, y: to.y))
        
        //Keep using the method addLineToPoint until you get to the one where about to close the path
        
        aPath.close()
        
        //If you want to stroke it with a red color
        color.set()
        
        aPath.stroke()
        
        //If you want to fill it as well
        //aPath.fill()
        
        /*if i < 10 {
         let testLabel = UILabel()
         testLabel.text = String(2.0 * Double(i) / 10.0 - 1)
         testLabel.frame = CGRect(x: w * CGFloat(i) / 10.0, y: h * 0.5, width: 100, height: 21)
         addSubview(testLabel)
         }*/
    }

    private func drawAxisAndGrid(h: CGFloat, w: CGFloat) {
        /*for i in 0...10 {
            let aPath = UIBezierPath()
            
            aPath.move(to: CGPoint(x: w * CGFloat(i) / 10.0, y: 0.0))
            
            aPath.addLine(to: CGPoint(x: w * CGFloat(i) / 10.0, y: h))
            
            //Keep using the method addLineToPoint until you get to the one where about to close the path
            
            aPath.close()
            
            //If you want to stroke it with a red color
            if i == 5 {
                UIColor.red.set()
            } else {
                UIColor.gray.set()
            }
            
            aPath.stroke()
            
            //If you want to fill it as well
            //aPath.fill()
            
            if i < 10 {
                let testLabel = UILabel()
                testLabel.text = String(2.0 * Double(i) / 10.0 - 1)
                testLabel.frame = CGRect(x: w * CGFloat(i) / 10.0, y: h * 0.5, width: 100, height: 21)
                addSubview(testLabel)
            }
        }*/
        
        for i in 0...10 {
            let aPath = UIBezierPath()
            
            aPath.move(to: CGPoint(x: 0.0, y: h * CGFloat(i) / 10.0))
            
            aPath.addLine(to: CGPoint(x: w, y: h * CGFloat(i) / 10.0))
            
            //Keep using the method addLineToPoint until you get to the one where about to close the path
            
            aPath.close()
            
            //If you want to stroke it with a red color
            if i == 5 {
                UIColor.red.set()
            } else {
                UIColor.gray.set()
            }
            
            aPath.stroke()
            
            //If you want to fill it as well
            //aPath.fill()
            
            /*if i != 5 && i < 10 {
                let testLabel = UILabel()
                testLabel.text = String(-(2.0 * Double(i) / 10.0 - 1))
                testLabel.frame = CGRect(x: w * 0.5, y: h * CGFloat(i) / 10.0, width: 100, height: 21)
                addSubview(testLabel)
            }*/
        }
        
        let yAxis = UIBezierPath()
        
        yAxis.move(to: CGPoint(x: toLocalCoords(x: 0, y: 0, h: h, w: w).localX, y: 0.0))
        
        yAxis.addLine(to: CGPoint(x: toLocalCoords(x: 0, y: 0, h: h, w: w).localX, y: h))
        
        //Keep using the method addLineToPoint until you get to the one where about to close the path
        
        yAxis.close()
        
        //If you want to stroke it with a red color
        //if i == 5 {
            UIColor.red.set()
        /*} else {
            UIColor.gray.set()
        }*/
        
        yAxis.stroke()
        
        for i in -10...10 {
            if i != 0 {
            let aPath = UIBezierPath()
            
            aPath.move(to: CGPoint(x: toLocalCoords(x: 0, y: 0, h: h, w: w).localX + w * CGFloat(i) / 10.0, y: 0.0))
            
            aPath.addLine(to: CGPoint(x: toLocalCoords(x: 0, y: 0, h: h, w: w).localX + w * CGFloat(i) / 10.0, y: h))
            
            //Keep using the method addLineToPoint until you get to the one where about to close the path
            
            aPath.close()
            
            //If you want to stroke it with a red color
            UIColor.gray.set()
            
            aPath.stroke()
            
            //If you want to fill it as well
            //aPath.fill()
            
            /*if i < 10 {
                let testLabel = UILabel()
                testLabel.text = String(2.0 * Double(i) / 10.0 - 1)
                testLabel.frame = CGRect(x: w * CGFloat(i) / 10.0, y: h * 0.5, width: 100, height: 21)
                addSubview(testLabel)
            }*/
            }
        }
        
        drawLine(from: (x: 0.0, y: 0.0), to: (x: 0.0, y: Double(h)), color: UIColor.gray)
        drawLine(from: (x: Double(w), y: 0.0), to: (x: Double(w), y: Double(h)), color: UIColor.gray)
    }
    
    override func draw(_ rect: CGRect) {
        let h = rect.height
        let w = rect.width
        
        fillRect(h: h, w: w)
        
        drawAxisAndGrid(h: h, w: w)
        
        var currentX = startFrom
        repeat {
            let currentY = f(currentX)
            
            var nextX = currentX + step
            
            if nextX > endAt {
                nextX = endAt
            }
            let nextY = f(nextX)
            
            let localCurrent = toLocalCoords(x: currentX, y: currentY, h: h, w: w)
            let localNext = toLocalCoords(x: nextX, y: nextY, h: h, w: w)
            drawLine(from: (x: Double(localCurrent.localX), y: Double(localCurrent.localY)), to: (x: Double(localNext.localX), y: Double(localNext.localY)), color: UIColor.magenta)
           
            currentX = nextX
            
        } while currentX < endAt
        
        //NSLog("drawRect has updated the view")
        
    }
    
}
