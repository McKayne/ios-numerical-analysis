//
//  TaskPicker.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 06.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation
import UIKit

class TaskPicker: NSObject, UIPickerViewDataSource {
    
    enum Task: String {
        
        static var count: Int {
            return 7
        }
        
        case Linear = "Linear algebra", Nonlinear = "Nonlinear solvers"
        case Interpolation = "Interpolation tasks", Approximaion = "Approximation tasks"
        case Cauchy = "Cauchy problem", Initial = "Initial boundary problem", PDE = "PDE solvers"
    }
    
    override init() {
        super.init()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Task.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch row {
        case 0:
            return Task.Linear.rawValue
        case 1:
            return Task.Nonlinear.rawValue
        case 2:
            return Task.Interpolation.rawValue
        case 3:
            return Task.Approximaion.rawValue
        case 4:
            return Task.Cauchy.rawValue
        case 5:
            return Task.Initial.rawValue
        case 6:
            return Task.PDE.rawValue
        default:
            return "Dummy"
        }
    }
}
