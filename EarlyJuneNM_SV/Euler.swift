//
//  Euler.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 12.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class EulerSolver {
    
    var u: [Double] = []
    
    func valueAt(x: Double) -> Double {
        return 0.0
    }
    
    init(f: (Double, Double) -> Double, method: Euler, startFrom: Double, endAt: Double, n: Int, startValue: Double) {
        //Euler.ExplicitEuler
        
        let h = (endAt - startFrom) / Double(n)
        
        switch method {
        case .ExplicitEuler:
            var x = startFrom, y = startFrom
            
            repeat {
                
                print("(\(x); \(y))")
                u.append(y)
                
                y += h * f(x, y)
                
                x += h
            } while x <= endAt
        case .EulerCauchy:
            var x = startFrom, y = startFrom
            
            repeat {
                
                print("(\(x); \(y))")
                u.append(y)
                
                let halfY = y + h * f(x, y)
                y += h * (f(x, y) + f(x, halfY)) / 2.0
                
                x += h
            } while x <= endAt
        case .ImprovedEulerMethod:
            var x = startFrom, y = startFrom
            
            repeat {
                
                print("(\(x); \(y))")
                u.append(y)
                
                let halfY = y + h * f(x, y) / 2.0
                y += h * f(x + h / 2.0, halfY)
                
                x += h
            } while x <= endAt
        default:
            break
        }
    }
}
