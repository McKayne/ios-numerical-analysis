//
//  IterationsSolver.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 12.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class IterationsSolver: Solver {
    
    override func findSolution(equation: @escaping (Double) -> Double) -> Double {
        
        func phi(x: Double) -> Double {
            return log(4 - 3 * x) / 2
        }
        
        var x: Double = (xEndAt - xStartFrom) / 2 + xStartFrom
        
        for _ in 0..<10 {
            x = phi(x: x)
        }
        
        return x
    }
    
    override init(xStartFrom: Double, xEndAt: Double) {
        super.init(xStartFrom: xStartFrom, xEndAt: xEndAt)
    }
}
