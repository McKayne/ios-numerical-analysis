//
//  FiniteDifference.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 12.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class FiniteDifference {
    
    var u: [Double] = []
    
    init(p: (Double) -> Double, q: (Double) -> Double, f: (Double) -> Double, startFrom: Double, endAt: Double, n: Int, startValue: Double, endValue: Double) {
        
        let h = (endAt - startFrom) / Double(n)
        
        var a: [[Double]] = [], b: [Double] = []
        for i in 1..<n {
            let x = h * Double(i)
            
            var aRow: [Double] = []
            
            for j in 1..<n {
                if j == i {
                    aRow.append(-2.0 + pow(h, 2.0) * q(x))
                } else if j == i + 1 {
                    aRow.append(1.0 + p(x) * h / 2.0)
                } else if j == i - 1 {
                    aRow.append(1.0 - p(x) * h / 2.0)
                } else {
                    aRow.append(0.0)
                }
            }
            
            a.append(aRow)
            
            if i == 1 {
                b.append(pow(h, 2.0) * f(x) - (1.0 - p(x) * h / 2.0) * startValue)
            } else if i == n - 1 {
                b.append(pow(h, 2.0) * f(x))
            } else {
                b.append(pow(h, 2.0) * f(x) + (1.0 + p(x) * h / 2.0) * endValue)
            }
        }
        
        print(a)
        print(b)
        
        u = [startValue] + Tridiag(a: a, b: b).findSolution()
        u += [endValue]
        for i in 0...n {
            print("x = \(u[i])")
        }
    }
}
