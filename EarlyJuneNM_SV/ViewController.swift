//
//  ViewController.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 06.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import UIKit
import Darwin
import Metal

let IS_SIMULATOR = true

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    enum Context {
        case Initial, Linear, Nonlinear, Interpolation, Approximation, Cauchy, PDE
        case GaussSolver, TridiagSolver, IterationsSolver, SeidelSolver, JacobiSolver
        case DichotomySolver, NewtonSolver, SimplifiedSolver, IterationsNonlinearSolver
        case Lagrange, Newton, Cubic
        case ExplicitEuler, EulerCauchy, ImprovedEulerMethod
        case Finite
        case TempPDE, Parabolic, Hyperbolic
    }
    
    var device: MTLDevice!
    #if false
        var metalLayer: CAMetalLayer!
    #endif
    
    let vertexData:[Float] = [
        0.0, 1.0, 0.0,
        -1.0, -1.0, 0.0,
        1.0, -1.0, 0.0]
    
    var vertexBuffer: MTLBuffer!
    
    var pipelineState: MTLRenderPipelineState!
    
    var context: Context = .Initial
    var appSwitchActive: Bool = true
    
    private var aMatrix: [[UITextField]] = [], bMatrix: [[UITextField]] = [], xMatrix: [[UITextField]] = []
    
    lazy private var aLabel: UILabel = UILabel()
    lazy private var bLabel: UILabel = UILabel()
    lazy private var xLabel: UILabel = UILabel()
    
    private var drawer: UIView?
    private var drawerIsHidden: Bool = true
    private var interpSize: Int = 5
    
    private var interpSizeLabel: UILabel = UILabel(), interpSizeText: UITextField = UITextField()
    private var interpValues: [Double] = []
    
    private var interpStartLabel = UILabel(), interpStartText = UITextField()
    private var interpEndLabel = UILabel(), interpEndText = UITextField()
    
    private var interpValuesText: [UITextField] = []
    
    private var interpXLabel: UILabel = UILabel(), interpXText: UITextField = UITextField()
    private var interpValueLabel: UILabel = UILabel(), interpValueText: UITextField = UITextField()
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var taskPicker: UIPickerView!
    
    @IBOutlet weak var appSwitchLabel: UILabel!
    @IBOutlet weak var appSwitch: UISwitch!
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //let taskPickerDataSource = TaskPicker()
        
        taskPicker.delegate = self
        taskPicker.dataSource = self
        
        /*let button = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 50))
        button.backgroundColor = .green
        button.setTitle("Test Button", for: .normal)
        self.view.addSubview(button)*/
        
        //addMatrixToView(startX: 100, startY: 200, rows: 3, cols: 3)
        
        
        
        
        //label.removeFromSuperview()
        
        
        
        #if false
            device = MTLCreateSystemDefaultDevice()
            
            metalLayer = CAMetalLayer()          // 1
            metalLayer.device = device           // 2
            metalLayer.pixelFormat = .bgra8Unorm // 3
            metalLayer.framebufferOnly = true    // 4
            metalLayer.frame = view.layer.frame  // 5
            view.layer.addSublayer(metalLayer)   // 6
            
            let dataSize = vertexData.count * MemoryLayout.size(ofValue: vertexData[0]) // 1
            vertexBuffer = device.makeBuffer(bytes: vertexData, length: dataSize, options: []) // 2
            
            
            // 1
            let defaultLibrary = device.newDefaultLibrary()!
            let fragmentProgram = defaultLibrary.makeFunction(name: "basic_fragment")
            let vertexProgram = defaultLibrary.makeFunction(name: "basic_vertex")
            
            // 2
            let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
            pipelineStateDescriptor.vertexFunction = vertexProgram
            pipelineStateDescriptor.fragmentFunction = fragmentProgram
            pipelineStateDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
            
            // 3
            pipelineState = try! device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
        #endif
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch context {
        case .Initial:
            return Task.count
        case .Linear:
            return Linear.count
        case .Nonlinear:
            if appSwitchActive {
                return Nonlinear.count
            } else {
                return 2
            }
        case .Interpolation:
            return Interpolation.count
        case .Approximation:
            return Approximation.count
        case .Cauchy:
            return Cauchy.count
        case .Finite:
            return 1
        case .PDE:
            return PDE.count
            
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch context {
        case .Initial:
            return DataSource.initialDataSource(row: row)
        case .Linear:
            return DataSource.linearDataSource(row: row)
        case .Nonlinear:
            return DataSource.nonlinearDataSource(row: row, appSwitchActive: appSwitchActive)
        case .Interpolation:
            return DataSource.interpolationDataSource(row: row)
        case .Approximation:
            return DataSource.approximationDataSource(row: row)
        case .Cauchy:
            return DataSource.cauchyDataSource(row: row)
        case .PDE:
            return DataSource.pdeDataSource(row: row)
            
        default:
            return "Dummy"
        }
    }
    
    func performLinearSolver() {
        var matrixA: [[Double]] = [], matrixB: [Double] = []
        for aMatrixRow in aMatrix {
            var matrixRow: [Double] = []
            for textField in aMatrixRow {
                matrixRow.append(Double(textField.text!)!)
            }
            matrixA.append(matrixRow)
        }
        for bMatrixRow in bMatrix {
            for textField in bMatrixRow {
                matrixB.append(Double(textField.text!)!)
            }
        }
        
        switch context {
        case .GaussSolver:
            let x = Gauss(a: matrixA, b: matrixB).findSolution()
            
            for xMatrixRow in xMatrix {
                for i in 0..<x.x.count {
                    xMatrixRow[i].text = String(x.x[i])
                }
            }
        case .TridiagSolver:
            let x = Tridiag(a: matrixA, b: matrixB).findSolution()
            
            for xMatrixRow in xMatrix {
                for i in 0..<x.count {
                    xMatrixRow[i].text = String(x[i])
                }
            }
        case .IterationsSolver:
            let x = Iterations(a: matrixA, b: matrixB).findSolution()
            
            for xMatrixRow in xMatrix {
                for i in 0..<x.count {
                    xMatrixRow[i].text = String(x[i])
                }
            }
        default:
            break
        }
    }
    
    /*override func draw(rect: CGRect) {
        
    }*/
    
    func notImplementedError() {
        let alert = UIAlertController(title: "Sorry", message: "Not implemented yet", preferredStyle: UIAlertControllerStyle.alert)
        
        /*dispatch_get_main_queue() {() -> () in self.present(alert, animated: true, completion: nil)}*/
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
    }
    
    @IBAction func taskNext(sender: AnyObject) {
        switch context {
        case .Initial:
            backButton.isHidden = false
            
            switch Task.intValue(nth: taskPicker.selectedRow(inComponent: 0)) {
            case Task.Linear.rawValue:
                context = .Linear
                taskPicker.reloadAllComponents()
            case Task.Nonlinear.rawValue:
                appSwitchLabel.isHidden = false
                appSwitch.isHidden = false
                
                context = .Nonlinear
                taskPicker.reloadAllComponents()
            case Task.Interpolation.rawValue:
                context = .Interpolation
                taskPicker.reloadAllComponents()
            case Task.Approximaion.rawValue:
                /*context = .Approximation
                taskPicker.reloadAllComponents()*/
                
                backButton.isHidden = true
                notImplementedError()
            case Task.Cauchy.rawValue:
                context = .Cauchy
                taskPicker.reloadAllComponents()
            case Task.Initial.rawValue:
                context = .Finite
                
                appSwitchLabel.isHidden = false
                appSwitch.isHidden = false
                
                taskPicker.isHidden = true
            case Task.PDE.rawValue:
                context = .PDE
                taskPicker.reloadAllComponents()
            default:
                break
            }
        case .Linear:
            switch taskPicker.selectedRow(inComponent: 0) {
            case 0:
                context = .GaussSolver
            case 1:
                context = .TridiagSolver
            case 2:
                context = .IterationsSolver
            case 3:
                context = .SeidelSolver
            case 4:
                context = .JacobiSolver
            default:
                break
            }
            
            
            //stackView.isHidden = true
            welcomeLabel.isHidden = true
            taskLabel.isHidden = true
            taskPicker.isHidden = true
            
            let matA = LinearUI.addMatrixA(view: self.view, startX: 100, startY: 200, rows: 3, cols: 3)
            aLabel = matA.label
            aMatrix = matA.aMatrix
            
            let matB = LinearUI.addMatrixB(view: self.view, startX: 100, startY: 400, cols: 3)
            bLabel = matB.label
            bMatrix = matB.aMatrix
            
            let matX = LinearUI.addMatrixX(view: self.view, startX: 100, startY: 500, cols: 3)
            xLabel = matX.label
            xMatrix = matX.aMatrix
        case .GaussSolver, .TridiagSolver, .IterationsSolver:
            performLinearSolver()
        case .SeidelSolver:
            var matrixA: [[Double]] = [], matrixB: [Double] = []
            for aMatrixRow in aMatrix {
                var matrixRow: [Double] = []
                for textField in aMatrixRow {
                    matrixRow.append(Double(textField.text!)!)
                }
                matrixA.append(matrixRow)
            }
            for bMatrixRow in bMatrix {
                for textField in bMatrixRow {
                    matrixB.append(Double(textField.text!)!)
                }
            }
            
            let x = Seidel(a: matrixA, b: matrixB).findSolution()
            
            for xMatrixRow in xMatrix {
                for i in 0..<x.count {
                    xMatrixRow[i].text = String(x[i])
                }
            }
        case .JacobiSolver:
            var matrixA: [[Double]] = [], matrixB: [Double] = []
            for aMatrixRow in aMatrix {
                var matrixRow: [Double] = []
                for textField in aMatrixRow {
                    matrixRow.append(Double(textField.text!)!)
                }
                matrixA.append(matrixRow)
            }
            for bMatrixRow in bMatrix {
                for textField in bMatrixRow {
                    matrixB.append(Double(textField.text!)!)
                }
            }
            
            let x = Jacobi(a: matrixA).findSolution()
            
            for xMatrixRow in xMatrix {
                for i in 0..<x.lambda.count {
                    xMatrixRow[i].text = String(x.lambda[i])
                }
            }
        case .Nonlinear:
            switch taskPicker.selectedRow(inComponent: 0) {
            case 0:
                print("Dich")
                
                let solver = Dichotomy(xStartFrom: 0.0, xEndAt: 1.0)
                print(solver.findSolution(equation: {(x: Double) -> Double in return exp(2 * x) + 3 * x - 4}))
            case 1:
                let solver = NewtonSolver(xStartFrom: 0.0, xEndAt: 1.0)
                print(solver.findSolution(equation: {(x: Double) -> Double in return exp(2 * x) + 3 * x - 4}))
            case 2:
                let solver = Simplified(xStartFrom: 0.0, xEndAt: 1.0)
                print(solver.findSolution(equation: {(x: Double) -> Double in return exp(2 * x) + 3 * x - 4}))
            case 3:
                let solver = IterationsSolver(xStartFrom: 0.0, xEndAt: 1.0)
                print(solver.findSolution(equation: {(x: Double) -> Double in return exp(2 * x) + 3 * x - 4}))
            default:
                notImplementedError()
            }
            print("NL")
        case .Interpolation:
            switch taskPicker.selectedRow(inComponent: 0) {
            case 0:
                context = .Lagrange
            case 1:
                context = .Newton
            case 2:
                context = .Cubic
            default:
                break
            }
            
            //stackView.isHidden = true
            welcomeLabel.isHidden = true
            taskLabel.isHidden = true
            taskPicker.isHidden = true
            
            //let matA = addMatrixA(startX: 100, startY: 200, rows: 3, cols: 3)
            //aLabel = matA.label
            //aMatrix = matA.aMatrix
            
            //let matB = addMatrixB(startX: 100, startY: 400, cols: 3)
            //bLabel = matB.label
            //bMatrix = matB.aMatrix
            
            interpSizeLabel.frame = CGRect(x: 100, y: 150, width: 200, height: 21)
            interpSizeLabel.textAlignment = NSTextAlignment.center
            interpSizeLabel.text = "Total values"
            self.view.addSubview(interpSizeLabel)

            interpSizeText.frame = CGRect(x: 300, y: 150, width: 50, height: 30)
            interpSizeText.borderStyle = .roundedRect
            interpSizeText.text = String(interpSize)
            self.view.addSubview(interpSizeText)
            
            interpStartLabel.frame = CGRect(x: 100, y: 200, width: 200, height: 21)
            interpStartLabel.textAlignment = NSTextAlignment.center
            interpStartLabel.text = "X from"
            self.view.addSubview(interpStartLabel)
            
            
            interpStartText.frame = CGRect(x: 300, y: 200, width: 50, height: 30)
            interpStartText.borderStyle = .roundedRect
            interpStartText.text = String(-1)
            self.view.addSubview(interpStartText)
            
            
            interpEndLabel.frame = CGRect(x: 100, y: 250, width: 200, height: 21)
            interpEndLabel.textAlignment = NSTextAlignment.center
            interpEndLabel.text = "X to"
            self.view.addSubview(interpEndLabel)
            
            
            interpEndText.frame = CGRect(x: 300, y: 250, width: 50, height: 30)
            interpEndText.borderStyle = .roundedRect
            interpEndText.text = String(1)
            self.view.addSubview(interpEndText)
            
            interpValues = []
            for _ in 0..<interpSize {
                interpValues.append(2.0 * (Double(arc4random()) / Double(UINT32_MAX) - 0.5))
            }
            
            interpValuesText = []
            var nextY: Int = 150
            for i in 0..<interpSize {
                let textField: UITextField = UITextField()
                textField.frame = CGRect(x: 450, y: nextY, width: 100, height: 30)
                textField.borderStyle = .roundedRect
                textField.text = String(interpValues[i])
                self.view.addSubview(textField)
                
                interpValuesText.append(textField)
                
                nextY += 50
            }
            
            
            
            interpXLabel.frame = CGRect(x: 100, y: 300, width: 200, height: 21)
            interpXLabel.textAlignment = NSTextAlignment.center
            interpXLabel.text = "X at"
            self.view.addSubview(interpXLabel)
            
            interpXText.frame = CGRect(x: 300, y: 300, width: 50, height: 30)
            interpXText.borderStyle = .roundedRect
            interpXText.text = String(0.0)
            self.view.addSubview(interpXText)
            
            interpValueLabel.frame = CGRect(x: 100, y: 350, width: 200, height: 21)
            interpValueLabel.textAlignment = NSTextAlignment.center
            interpValueLabel.text = "Interpolated value"
            self.view.addSubview(interpValueLabel)
            
            interpValueText.frame = CGRect(x: 300, y: 350, width: 100, height: 30)
            interpValueText.borderStyle = .roundedRect
            //interpValueText.text = String(0.0)
            self.view.addSubview(interpValueText)
            
            //startX: 100, startY: 200
            
            /*let matX = LinearUI.addMatrixX(view: self.view, startX: 100, startY: 500, cols: 3)
            xLabel = matX.label
            xMatrix = matX.aMatrix*/
            switch context {
            case .Lagrange:
                let lag = Lagrange(f: interpValues, xStart: -1, xEnd: 1)
                
                interpValueText.text = String(lag.valueAt(x: Double(interpXText.text!)!))
                
                if drawerIsHidden {
                    drawer = Drawing(frame: CGRect(
                        origin: CGPoint(x: 100, y: 500),
                        size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in return lag.valueAt(x:x)}, startFrom: -1.0, endAt: 1.0, step: 0.1)
                    self.view.addSubview(drawer!)
                    drawerIsHidden = false
                }
            case .Newton:
                let newton = Newton(f: interpValues, xStart: -1, xEnd: 1)
                
                interpValueText.text = String(newton.valueAt(x: Double(interpXText.text!)!))
                
                if drawerIsHidden {
                    drawer = Drawing(frame: CGRect(
                        origin: CGPoint(x: 100, y: 500),
                        size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in return newton.valueAt(x:x)}, startFrom: -1.0, endAt: 1.0, step: 0.1)
                    self.view.addSubview(drawer!)
                    drawerIsHidden = false
                }
            case .Cubic:
                let cubic = Cubic(f: interpValues, xStart: -1, xEnd: 1)
                
                interpValueText.text = String(cubic.valueAt(x: Double(interpXText.text!)!))
                
                if drawerIsHidden {
                    drawer = Drawing(frame: CGRect(
                        origin: CGPoint(x: 100, y: 500),
                        size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in return cubic.valueAt(x:x)}, startFrom: -1.0, endAt: 1.0, step: 0.1)
                    self.view.addSubview(drawer!)
                    drawerIsHidden = false
                }
            default:
                break
            }
        case .Lagrange:
            let lag = Lagrange(f: interpValues, xStart: -1, xEnd: 1)
            
            interpValueText.text = String(lag.valueAt(x: Double(interpXText.text!)!))
            
            if drawerIsHidden {
                drawer = Drawing(frame: CGRect(
                origin: CGPoint(x: 100, y: 500),
                size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in return lag.valueAt(x:x)}, startFrom: -1.0, endAt: 1.0, step: 0.1)
                self.view.addSubview(drawer!)
                drawerIsHidden = false
            }
        case .Newton:
            print("Newton")
            let newton = Newton(f: interpValues, xStart: -1, xEnd: 1)
            
            interpValueText.text = String(newton.valueAt(x: Double(interpXText.text!)!))
            
            if drawerIsHidden {
            drawer = Drawing(frame: CGRect(
                origin: CGPoint(x: 100, y: 500),
                size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in return newton.valueAt(x:x)}, startFrom: -1.0, endAt: 1.0, step: 0.1)
            self.view.addSubview(drawer!)
            drawerIsHidden = false
            }
        case .Cubic:
            let cubic = Cubic(f: interpValues, xStart: -1, xEnd: 1)
            
            interpValueText.text = String(cubic.valueAt(x: Double(interpXText.text!)!))
            
            if drawerIsHidden {
            drawer = Drawing(frame: CGRect(
                origin: CGPoint(x: 100, y: 500),
                size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in return cubic.valueAt(x:x)}, startFrom: -1.0, endAt: 1.0, step: 0.1)
            self.view.addSubview(drawer!)
            drawerIsHidden = false
            }
        case .Cauchy:
            taskPicker.isHidden = true
            welcomeLabel.isHidden = true
            taskLabel.isHidden = true
            
            switch taskPicker.selectedRow(inComponent: 0) {
            case 0:
                context = .ExplicitEuler
            case 2:
                context = .EulerCauchy
            case 5:
                context = .ImprovedEulerMethod
            default:
                notImplementedError()
            }
            
            interpSizeLabel.frame = CGRect(x: 100, y: 150, width: 200, height: 21)
            interpSizeLabel.textAlignment = NSTextAlignment.center
            interpSizeLabel.text = "Total values"
            self.view.addSubview(interpSizeLabel)
            
            interpSizeText.frame = CGRect(x: 300, y: 150, width: 50, height: 30)
            interpSizeText.borderStyle = .roundedRect
            interpSizeText.text = String(interpSize)
            self.view.addSubview(interpSizeText)
            
            interpStartLabel.frame = CGRect(x: 100, y: 200, width: 200, height: 21)
            interpStartLabel.textAlignment = NSTextAlignment.center
            interpStartLabel.text = "X from"
            self.view.addSubview(interpStartLabel)
            
            
            interpStartText.frame = CGRect(x: 300, y: 200, width: 50, height: 30)
            interpStartText.borderStyle = .roundedRect
            interpStartText.text = String(0.0)
            self.view.addSubview(interpStartText)
            
            
            interpEndLabel.frame = CGRect(x: 100, y: 250, width: 200, height: 21)
            interpEndLabel.textAlignment = NSTextAlignment.center
            interpEndLabel.text = "X to"
            self.view.addSubview(interpEndLabel)
            
            
            interpEndText.frame = CGRect(x: 300, y: 250, width: 50, height: 30)
            interpEndText.borderStyle = .roundedRect
            interpEndText.text = String(0.5)
            self.view.addSubview(interpEndText)
            
            interpXLabel.frame = CGRect(x: 100, y: 300, width: 200, height: 21)
            interpXLabel.textAlignment = NSTextAlignment.center
            interpXLabel.text = "X at"
            self.view.addSubview(interpXLabel)
            
            interpXText.frame = CGRect(x: 300, y: 300, width: 50, height: 30)
            interpXText.borderStyle = .roundedRect
            interpXText.text = String(0.0)
            self.view.addSubview(interpXText)
            
            interpValueLabel.frame = CGRect(x: 100, y: 350, width: 200, height: 21)
            interpValueLabel.textAlignment = NSTextAlignment.center
            interpValueLabel.text = "Value"
            self.view.addSubview(interpValueLabel)
            
            interpValueText.frame = CGRect(x: 300, y: 350, width: 100, height: 30)
            interpValueText.borderStyle = .roundedRect
            //interpValueText.text = String(0.0)
            self.view.addSubview(interpValueText)
            
        case .ExplicitEuler:
            let euler = EulerSolver(f: {(x: Double, y: Double) in return pow(y + x, 2.0)}, method: Euler.ExplicitEuler, startFrom: Double(interpStartText.text!)!, endAt: Double(interpEndText.text!)!, n: Int(interpSizeText.text!)!, startValue: Double(interpXText.text!)!)
            
            if drawerIsHidden {
                drawer = Drawing(frame: CGRect(
                origin: CGPoint(x: 100, y: 500),
                size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in return x}, startFrom: -1.0, endAt: 3.0, step: 0.1)
                self.view.addSubview(drawer!)
                drawerIsHidden = false
            }
        case .EulerCauchy:
            let euler = EulerSolver(f: {(x: Double, y: Double) in return pow(y + x, 2.0)}, method: Euler.EulerCauchy, startFrom: Double(interpStartText.text!)!, endAt: Double(interpEndText.text!)!, n: 10, startValue: Double(interpXText.text!)!)
            
            if drawerIsHidden {
                drawer = Drawing(frame: CGRect(
                    origin: CGPoint(x: 100, y: 500),
                    size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in let i = Int(x / 0.05); return euler.u[i] * 20.0}, startFrom: 0.0, endAt: 0.5, step: 0.05)
                self.view.addSubview(drawer!)
                drawerIsHidden = false
            }
        case .ImprovedEulerMethod:
            let euler = EulerSolver(f: {(x: Double, y: Double) in return pow(y + x, 2.0)}, method: Euler.ImprovedEulerMethod, startFrom: Double(interpStartText.text!)!, endAt: Double(interpEndText.text!)!, n: 10, startValue: Double(interpXText.text!)!)
            
            if drawerIsHidden {
                drawer = Drawing(frame: CGRect(
                    origin: CGPoint(x: 100, y: 500),
                    size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in let i = Int(x / 0.05); return euler.u[i] * 20.0}, startFrom: 0.0, endAt: 0.5, step: 0.05)
                self.view.addSubview(drawer!)
                drawerIsHidden = false
            }
        case .Finite:
            let finite = FiniteDifference(p: {(x: Double) -> Double in return -x}, q: {(x: Double) -> Double in return -1.0}, f: {(x: Double) -> Double in return 0.0}, startFrom: 0.0, endAt: 1.0, n: 10, startValue: 0.0, endValue: 1.0)
            
            if drawerIsHidden {
                drawer = Drawing(frame: CGRect(
                    origin: CGPoint(x: 100, y: 500),
                    size: CGSize(width: 400, height: 200)), f: {(x: Double) -> Double in let i = Int(x / 0.1); return finite.u[i] / 11.0}, startFrom: 0.0, endAt: 1.0, step: 0.1)
                self.view.addSubview(drawer!)
                drawerIsHidden = false
            }
            print("finite")
        case .PDE:
            
            welcomeLabel.isHidden = true
            taskLabel.isHidden = true
            taskPicker.isHidden = true
            
            switch taskPicker.selectedRow(inComponent: 0) {
            case 0:
                context = .Parabolic
            case 1:
                context = .Hyperbolic
            default:
                notImplementedError()
            }
        case .TempPDE:
            print("pde")
        case .Parabolic:
            let pde = PDESolver(f: {(x: Double) in return sin(2.0 * 3.14 * x)}, method: PDE.Parabolic, startFrom: 0.0, endAt: 1.0, n: 10, startValue: 0.0)
        case .Hyperbolic:
            let pde = PDESolver(f: {(x: Double) in return sin(x)}, derivative: {(x: Double) in return -cos(x)}, method: PDE.Hyperbolic, startFrom: 0.0, endAt: 1.0, n: 10, startValue: 0.0)
        default:
            break
        }
    }
    
    @IBAction func taskBack(sender: AnyObject) {
        switch context {
        case .Initial:
            break
        case .Linear, .Nonlinear, .Interpolation, .Approximation, .Cauchy, .PDE:
            appSwitchLabel.isHidden = true
            appSwitch.isHidden = true
            
            backButton.isHidden = true
            
            context = .Initial
            
            taskPicker.isHidden = false
            taskPicker.reloadAllComponents()
        case .GaussSolver, .TridiagSolver, .IterationsSolver, .SeidelSolver, .JacobiSolver:
            context = .Linear
            
            aLabel.removeFromSuperview()
            bLabel.removeFromSuperview()
            xLabel.removeFromSuperview()
            for aMatrixRow in aMatrix {
                for textField in aMatrixRow {
                    textField.removeFromSuperview()
                }
            }
            for bMatrixRow in bMatrix {
                for textField in bMatrixRow {
                    textField.removeFromSuperview()
                }
            }
            for xMatrixRow in xMatrix {
                for textField in xMatrixRow {
                    textField.removeFromSuperview()
                }
            }
            
            welcomeLabel.isHidden = false
            taskLabel.isHidden = false
            taskPicker.isHidden = false
        case .DichotomySolver, .NewtonSolver, .SimplifiedSolver, .IterationsNonlinearSolver:
            break
        case .Lagrange, .Newton, .Cubic:
            context = .Interpolation
            
            welcomeLabel.isHidden = false
            taskLabel.isHidden = false
            taskPicker.isHidden = false
            
            interpSizeLabel.removeFromSuperview()
            interpSizeText.removeFromSuperview()
            
            interpStartLabel.removeFromSuperview()
            interpStartText.removeFromSuperview()
            
            interpEndLabel.removeFromSuperview()
            interpEndText.removeFromSuperview()
            
            for textField in interpValuesText {
                textField.removeFromSuperview()
            }
            
            interpXLabel.removeFromSuperview()
            interpXText.removeFromSuperview()
            
            interpValueLabel.removeFromSuperview()
            interpValueText.removeFromSuperview()
            
            if !drawerIsHidden {
                drawer!.removeFromSuperview()
                drawerIsHidden = true
            }
            break
        case .ExplicitEuler, .EulerCauchy, .ImprovedEulerMethod:
            break
        case .Finite:
            break
        case .TempPDE, .Parabolic, .Hyperbolic:
            break
        }
    }
    
    @IBAction func toggleSwitch(sender: AnyObject) {
        appSwitchActive = appSwitch.isOn
        taskPicker.reloadAllComponents()
    }
}

