//
//  PDESolver.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 12.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class PDESolver {
    
    init(f: (Double) -> Double, derivative: (Double) -> Double, method: PDE, startFrom: Double, endAt: Double, n: Int, startValue: Double) {
        
        let hx = (endAt - startFrom) / Double(n)
        let ht = 0.001
        
        switch method {
        case .Hyperbolic:
            var u0: [Double] = [], u1: [Double] = [], u: [Double] = []
            for i in 0...n {
                let x = hx * Double(i)
                print("x = \(x)")
                
                u0.append(f(x))
                print("f = \(u0[i])")
                
                u.append(0.0)
            }
            for i in 0...n {
                let x = hx * Double(i)
                print("x = \(x)")
                
                u1.append(derivative(x) * ht + u0[i])
                print("f1 = \(u1[i])")
            }
            
            let a = 1.0
            
            for i in 1..<n {
                u[i] = u1[i - 1] * (pow(ht, 2.0) * pow(a, 2.0) - pow(hx, 2.0)) / pow(hx, 2.0) + u1[i] * (2.0 * pow(hx, 2.0) - 2.0 * pow(ht, 2.0) * pow(a, 2.0)) / pow(hx, 2.0) + u1[i + 1] * pow(ht, 2.0) * pow(a, 2.0) / pow(hx, 2.0) - u0[i]
            }
            
            for i in 0...n {
                print("New f = \(u[i])")
            }
        default:
            break
        }
    }
    
    init(f: (Double) -> Double, method: PDE, startFrom: Double, endAt: Double, n: Int, startValue: Double) {
        
        let hx = (endAt - startFrom) / Double(n)
        let ht = 0.001
        
        switch method {
        case .Parabolic:
            var u: [Double] = []
            for i in 0...n {
                let x = hx * Double(i)
                print("x = \(x)")
                
                u.append(f(x))
                print("f = \(u[i])")
            }
            
            let a = 1.0
            
            for i in 1..<n {
                u[i] = u[i - 1] * (ht * pow(a, 2.0) / pow(hx, 2.0)) + u[i] * ((pow(hx, 2.0) - ht * pow(a, 2.0)) / pow(hx, 2.0)) + u[i + 1] * (ht * pow(a, 2.0) / pow(hx, 2.0))
            }
            
            for i in 0...n {
                print("New f = \(u[i])")
            }
        default:
            break
        }
    }
}
