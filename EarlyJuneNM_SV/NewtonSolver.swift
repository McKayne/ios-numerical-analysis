//
//  NewtonSolver.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 12.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class NewtonSolver: Solver {
    
    override func findSolution(equation: @escaping (Double) -> Double) -> Double {
        
        func derivative(x: Double) -> Double {
            let h: Double = pow(10, -4)
            return (equation(x + h) - equation(x)) / h
        }
        
        func secondDerivative(x: Double) -> Double {
            let h: Double = pow(10, -4)
            return (derivative(x: x + h) - derivative(x: x)) / h
        }
        
        var x = xEndAt, oldX: Double
        
        repeat {
            let cond = equation(x) * secondDerivative(x: x)
            //print("cond = \(cond)")
            //print(derivative(x: x))
            //print(equation(0.6))
            //print(equation(0.6 + pow(10, -4)))
            //print(equation(0.6 + pow(10, -4)) - equation(0.6))
            
            oldX = x
            x -= equation(x) / derivative(x: x)
        } while abs(x - oldX) >= pow(10, -3)
        
        return x
    }
    
    override init(xStartFrom: Double, xEndAt: Double) {
        super.init(xStartFrom: xStartFrom, xEndAt: xEndAt)
    }
}
