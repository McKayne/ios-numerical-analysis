//
//  Gauss.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 08.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Gauss {
    
    private var a: [[Double]]
    private var b: [Double]
    private var n: Int
 
    func findSolution() -> (x: [Double], det: Double) {
        var x: [Double] = []
        
        var matA = a, matB = b
        
        print(matA)
        
        for i in 0..<(n - 1) {
            for row in (i + 1)..<n {
                let nth: Double = -matA[row][i] / matA[i][i]
                for col in i..<n {
                    matA[row][col] += matA[i][col] * nth
                }
                matB[row] += matB[i] * nth
            }
        }
        
        print(matA)
        
        var det: Double = 1
        for i in 0..<n {
            det *= matA[i][i]
        }
        
        for i in (0...n - 1).reversed() {
            var nth: Double = matB[i]
            for j in ((i + 1)..<n).reversed() {
                nth -= x[n - 1 - j] * matA[i][j]
            }
            nth /= matA[i][i]
            x.append(nth)
        }
        
        return (x, det)
    }
    
    init(a: [[Double]], b: [Double]) {
        self.a = a
        self.b = b
        n = a.count
    }
}
