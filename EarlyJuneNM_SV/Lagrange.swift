//
//  Lagrange.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 09.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Lagrange {
   
    private var f: [Double], n: Int, h: Double, xStart: Double, xEnd: Double
    
    private func xNth(i: Int) -> Double {
        return xStart + Double(i) * h
    }
    
    private func omega(x: Double) -> Double {
        var o = x - xStart
        for i in 1...n {
            o *= (x - xNth(i: i))
        }
        return o
    }
    
    private func omegaB(i: Int) -> Double {
        var o = 1.0
        for j in 0...n {
            if i != j {
                o *= (xNth(i: i) - xNth(i: j))
            }
        }
        
        /*if o == 0.0 {
            return 1.0
        }*/
        
        return o
    }
    
    func valueAt(x: Double) -> Double {
        var l = 0.0
        for i in 0...n {
            var p = 1.0
            for j in 0...n {
                if i != j {
                    p *= ((x - xNth(i: j)) / (xNth(i: i) - xNth(i: j)))
                }
            }
            l += f[i] * p
        }
        
        /*for i in 0...n {
            var div = ((x - xNth(i: i)) * omegaB(i: i))
            print("div \(div)")
            l += f[i] * omega(x: x) / ((x - xNth(i: i)) * omegaB(i: i))
        }*/
        
        return l
    }
    
    init(f: [Double], xStart: Double, xEnd: Double) {
        self.f = f
        self.xStart = xStart
        self.xEnd = xEnd
        n = f.count - 1
        h = (xEnd - xStart) / Double(n)
    }
}
