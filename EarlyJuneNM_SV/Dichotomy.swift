//
//  Dichotomy.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 12.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Dichotomy: Solver {
    
    private func sign(d: Double) -> Double {
        if d > 0 {
            return 1
        } else if d < 0 {
            return -1
        } else {
            return 0
        }
    }
    
    override func findSolution(equation: @escaping (Double) -> Double) -> Double {
        var x = (xEndAt - xStartFrom) / 2 + xStartFrom
        
        repeat {
            let f = equation(x)
            if f == 0 {
                return x
            } else if sign(d: equation(xStartFrom)) != sign(d: equation(x)) {
                xEndAt = x
                x = (xEndAt - xStartFrom) / 2 + xStartFrom
            } else {
                xStartFrom = x
                x = (xEndAt - xStartFrom) / 2 + xStartFrom
            }
        } while (xEndAt - xStartFrom) > 2 * pow(10, -3)
        
        return x
    }
    
    override init(xStartFrom: Double, xEndAt: Double) {
        super.init(xStartFrom: xStartFrom, xEndAt: xEndAt)
    }
}
