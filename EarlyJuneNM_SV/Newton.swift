//
//  Newton.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 09.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Newton {
    
    private var f: [Double], n: Int, h: Double, xStart: Double, xEnd: Double
    
    private func xNth(i: Int) -> Double {
        return xStart + Double(i) * h
    }
    
    private func diff(startOffset: Int, endOffset: Int) -> Double {
        print(n - endOffset - startOffset)
        if n - endOffset - startOffset > 2 {
            return (diff(startOffset: startOffset, endOffset: endOffset + 1) - diff(startOffset: startOffset + 1, endOffset: endOffset)) / (xNth(i: startOffset) - xNth(i: n - endOffset))
        } else {
            return (f[startOffset] - f[n - endOffset]) / (xNth(i: startOffset) - xNth(i: n - endOffset))
        }
    }
    
    func valueAt(x: Double) -> Double {
        var nn = f[0]
        for i in 0..<n {
            var d = (x - xNth(i: 0))
            
            if i >= 1 {
                for j in 1...i {
                    d *= (x - xNth(i: j))
                }
            }
            
            d *= diff(startOffset: 0, endOffset: n - i - 1)
            
            nn += d
        }
        return nn
    }
    
    init(f: [Double], xStart: Double, xEnd: Double) {
        self.f = f
        self.xStart = xStart
        self.xEnd = xEnd
        n = f.count - 1
        h = (xEnd - xStart) / Double(n)
    }
}
