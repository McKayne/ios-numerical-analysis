//
//  Cubic.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 08.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Cubic {
    
    private var f: [Double], xStart: Double, xEnd: Double, n: Int, h: Double
    private var a: [Double] = [], b: [Double] = [], c: [Double] = [], d: [Double] = []
    
    func valueAt(x: Double) -> Double {
        let i = indexAt(x: x)
        print("fff \(x) \(i)")
        let diff: Double = x - (xStart + h * Double(i))
        return a[i] + b[i] * diff + c[i] * pow(diff, 2.0) + d[i] * pow(diff, 3.0)
    }
    
    private func indexAt(x: Double) -> Int {
        var i: Int = 0
        while x > xStart + Double(i + 1) * h {
            i += 1
        }
        return i
    }
    
    private func initD(c: [Double]) -> [Double] {
        var d: [Double] = []
        for i in 0..<n - 1 {
            d.append((c[i + 1] - c[i]) / (3.0 * h))
        }
        d.append(-c[n - 1] / (3.0 * h))
        return d
    }
    
    private func initB(c: [Double]) -> [Double] {
        var b: [Double] = []
        for i in 0..<n - 1 {
            b.append((f[i + 1] - f[i]) / h - h * (c[i + 1] + 2.0 * c[i]) / 3.0)
        }
        b.append((f[n - 1] - f[n - 2]) / h - 2.0 * h * c[n - 1] / 3.0)
        return b
    }
    
    private func initC() -> [Double] {
        var a: [[Double]] = [], b: [Double] = []
        for i in 0..<n - 1 {
            var aRow: [Double] = []
            for j in 0..<n - 1 {
                if i == j {
                    aRow.append(2.0 * (h + h))
                } else if j == i - 1 || j == i + 1 {
                    aRow.append(h)
                } else {
                    aRow.append(0.0)
                }
            }
            a.append(aRow)
            b.append(3.0 * ((f[i + 2] - f[i + 1]) / h - (f[i + 1] - f[i]) / h))
        }
        return [0.0] + Tridiag(a: a, b: b).findSolution()
    }
    
    private func initA() -> [Double] {
        var a: [Double] = []
        for i in 1...n {
            a.append(f[i - 1])
        }
        return a
    }
    
    private func initializeSpline() {
        a = initA()
        c = initC()
        b = initB(c: c)
        d = initD(c: c)
    }
    
    init(f: [Double], xStart: Double, xEnd: Double) {
        self.f = f
        self.xStart = xStart
        self.xEnd = xEnd
        n = f.count - 1
        h = (xEnd - xStart) / Double(n)
        
        initializeSpline()
    }
}
