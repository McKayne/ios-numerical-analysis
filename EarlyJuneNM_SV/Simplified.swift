//
//  Simplified.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 12.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

class Simplified: Solver {
    
    override func findSolution(equation: @escaping (Double) -> Double) -> Double {
        var h: Double = pow(10, -1), x = xEndAt - h, x0: Double = xEndAt
        
        var oldX = x0
        repeat {
            
            oldX = x
            x -= equation(x) * (x - x0) / (equation(x) - equation(x0))
            x0 = oldX
        } while abs(x - x0) >= pow(10, -3)
        
        return x
    }
    
    override init(xStartFrom: Double, xEndAt: Double) {
        super.init(xStartFrom: xStartFrom, xEndAt: xEndAt)
    }
}
