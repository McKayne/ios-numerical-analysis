//
//  DataSource.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 06.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation

enum Task: String {
    
    static var count: Int {
        return 7
    }
    
    static func intValue(nth: Int) -> String {
        switch nth {
        case 0:
            return Task.Linear.rawValue
        case 1:
            return Task.Nonlinear.rawValue
        case 2:
            return Task.Interpolation.rawValue
        case 3:
            return Task.Approximaion.rawValue
        case 4:
            return Task.Cauchy.rawValue
        case 5:
            return Task.Initial.rawValue
        case 6:
            return Task.PDE.rawValue
        default:
            return "Dummy"
        }
    }
    
    case Linear = "Linear algebra", Nonlinear = "Nonlinear solvers"
    case Interpolation = "Interpolation tasks", Approximaion = "Approximation tasks"
    case Cauchy = "Cauchy problem", Initial = "Initial boundary problem", PDE = "PDE solvers"
}

enum Linear: String {
    
    static var count: Int {
        return 6
    }
    
    case Gauss = "Gauss method solver", Tridiag = "Tridiagonal method solver"
    case Simple = "Simple iterations method solver", Seidel = "Seidel method solver"
    case Jacobi = "Rotational eigenvalues and eigenvectors solver", QR = "QR eigenvalues and eigenvectors solver"
}

enum Nonlinear: String {
    
    static var count: Int {
        return 4
    }
    
    case Dichotomy = "Dichotomy method solver", Newton = "Newton method solver"
    case Simplified = "Simplified Newton method solver", Iterations = "Simple iterations method solver"
}

enum Interpolation: String {
    
    static var count: Int {
        return 3
    }
    
    case Lagrange = "Lagrange polynomial interpolation"
    case Newton = "Newton polynomial interpolation"
    case Cubic = "Cubic spline interpolation"
}

enum Approximation: String {
    
    static var count: Int {
        return 2
    }
    
    case Derivative = "Derivative approximation"
    case Integral = "Integral approximation"
}

enum Cauchy: String {
    
    static var count: Int {
        return 8
    }
    
    case ThirdOrderRungeKutta = "Runge-Kutta method (3rd order)", RungeKutta = "Runge-Kutta method (4th order)"
}

enum Euler: String {
    case ExplicitEuler = "Explicit Euler method", ImplicitEuler = "Implicit Euler method"
    case EulerCauchy = "Euler-Cauchy method", ImplicitEulerCauchy = "Implicit Euler-Cauchy method", IterationalEulerCauchy = "Iterational Euler-Cauchy method", ImprovedEulerMethod = "Improved Euler method"
}

enum PDE: String {
    
    static var count: Int {
        return 4
    }
    
    case Parabolic = "Parabolic PDE solver", Hyperbolic = "Hyperbolic PDE solver", Elliptic = "Elliptic PDE solver"
    case TwoDim = "2D PDE solver"
}

class DataSource {
    
    static func initialDataSource(row: Int) -> String {
        switch row {
        case 0:
            return Task.Linear.rawValue
        case 1:
            return Task.Nonlinear.rawValue
        case 2:
            return Task.Interpolation.rawValue
        case 3:
            return Task.Approximaion.rawValue
        case 4:
            return Task.Cauchy.rawValue
        case 5:
            return Task.Initial.rawValue
        case 6:
            return Task.PDE.rawValue
        default:
            return "Dummy"
        }
    }
    
    static func linearDataSource(row: Int) -> String {
        switch row {
        case 0:
            return Linear.Gauss.rawValue
        case 1:
            return Linear.Tridiag.rawValue
        case 2:
            return Linear.Simple.rawValue
        case 3:
            return Linear.Seidel.rawValue
        case 4:
            return Linear.Jacobi.rawValue
        case 5:
            return Linear.QR.rawValue
        default:
            return "Dummy"
        }
    }
    
    static func nonlinearDataSource(row: Int, appSwitchActive: Bool) -> String {
        if appSwitchActive {
            switch row {
            case 0:
                return Nonlinear.Dichotomy.rawValue
            case 1:
                return Nonlinear.Newton.rawValue
            case 2:
                return Nonlinear.Simplified.rawValue
            case 3:
                return Nonlinear.Iterations.rawValue
            default:
                return "Dummy"
            }
        } else {
            switch row {
            case 0:
                return Nonlinear.Newton.rawValue
            case 1:
                return Nonlinear.Iterations.rawValue
            default:
                return "Dummy"
            }
        }
    }
    
    static func interpolationDataSource(row: Int) -> String {
        switch row {
        case 0:
            return Interpolation.Lagrange.rawValue
        case 1:
            return Interpolation.Newton.rawValue
        case 2:
            return Interpolation.Cubic.rawValue
        default:
            return "Dummy"
        }
    }
    
    static func approximationDataSource(row: Int) -> String {
        switch row {
        case 0:
            return Approximation.Derivative.rawValue
        case 1:
            return Approximation.Integral.rawValue
        default:
            return "Dummy"
        }
    }
    
    static func cauchyDataSource(row: Int) -> String {
        switch row {
        case 0:
            return Euler.ExplicitEuler.rawValue
        case 1:
            return Euler.ImplicitEuler.rawValue
        case 2:
            return Euler.EulerCauchy.rawValue
        case 3:
            return Euler.ImplicitEulerCauchy.rawValue
        case 4:
            return Euler.IterationalEulerCauchy.rawValue
        case 5:
            return Euler.ImprovedEulerMethod.rawValue
        case 6:
            return Cauchy.ThirdOrderRungeKutta.rawValue
        case 7:
            return Cauchy.RungeKutta.rawValue
        default:
            return "Dummy"
        }
    }
    
    static func pdeDataSource(row: Int) -> String {
        switch row {
        case 0:
            return PDE.Parabolic.rawValue
        case 1:
            return PDE.Hyperbolic.rawValue
        case 2:
            return PDE.Elliptic.rawValue
        case 3:
            return PDE.TwoDim.rawValue
        default:
            return "Dummy"
        }
    }
}
