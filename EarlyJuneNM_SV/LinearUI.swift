//
//  LinearUI.swift
//  EarlyJuneNM_SV
//
//  Created by для интернета on 08.06.18.
//  Copyright © 2018 для интернета. All rights reserved.
//

import Foundation
import UIKit

class LinearUI {
    
    static func addMatrixA(view: UIView, startX: Int, startY: Int, rows: Int, cols: Int) -> (label: UILabel, aMatrix: [[UITextField]]) {
        let label: UILabel = UILabel()
        label.frame = CGRect(x: 100, y: 150, width: 200, height: 21)
        //label.backgroundColor = .black
        //label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        label.text = "Matrix A"
        view.addSubview(label)
        
        var aMatrix: [[UITextField]] = []
        var nextY = startY
        for nthRow in 0..<rows {
            var nextX = startX
            var aMatrixRow: [UITextField] = []
            
            for nthCol in 0..<cols {
                let textField: UITextField = UITextField()
                textField.frame = CGRect(x: nextX, y: nextY, width: 50, height: 30)
                //textField.backgroundColor = .gray
                
                if nthCol == nthRow {
                    textField.text = String(2)
                } else if nthRow == nthCol + 1 || nthRow == nthCol - 1 {
                    textField.text = String(1)
                } else {
                    textField.text = String(0)
                }
                
                //textField.text = String((nthRow + 1) + nthCol)
                textField.borderStyle = .roundedRect
                view.addSubview(textField)
                
                aMatrixRow.append(textField)
                
                nextX += 60
            }
            
            aMatrix.append(aMatrixRow)
            nextY += 40
        }
        
        /*aMatrix[0][0].text = "2"
         aMatrix[0][1].text = "1"
         aMatrix[0][2].text = "0"
         
         aMatrix[1][0].text = "1"
         aMatrix[1][1].text = "2"
         aMatrix[1][2].text = "1"
         
         aMatrix[2][0].text = "0"
         aMatrix[2][1].text = "1"
         aMatrix[2][2].text = "2"*/
        
        return (label: label, aMatrix: aMatrix)
    }
    
    static func addMatrixB(view: UIView, startX: Int, startY: Int, cols: Int) -> (label: UILabel, aMatrix: [[UITextField]]) {
        let label: UILabel = UILabel()
        label.frame = CGRect(x: 100, y: 350, width: 200, height: 21)
        //label.backgroundColor = .black
        //label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        label.text = "Matrix B"
        view.addSubview(label)
        
        var aMatrix: [[UITextField]] = []
        var nextY = startY
        
        var nextX = startX
        var aMatrixRow: [UITextField] = []
        
        for nthCol in 0..<cols {
            let textField: UITextField = UITextField()
            textField.frame = CGRect(x: nextX, y: startY, width: 50, height: 30)
            //textField.backgroundColor = .gray
            
            if nthCol == 0 || nthCol == cols - 1 {
                textField.text = String(3)
            } else {
                textField.text = String(4)
            }
            
            textField.borderStyle = .roundedRect
            view.addSubview(textField)
            
            aMatrixRow.append(textField)
            
            nextX += 60
        }
        
        aMatrix.append(aMatrixRow)
        nextY += 40
        
        /*aMatrix[0][0].text = "3"
         aMatrix[0][1].text = "4"
         aMatrix[0][2].text = "3"*/
        
        return (label: label, aMatrix: aMatrix)
    }
    
    static func addMatrixX(view: UIView, startX: Int, startY: Int, cols: Int) -> (label: UILabel, aMatrix: [[UITextField]]) {
        let label: UILabel = UILabel()
        label.frame = CGRect(x: 100, y: 450, width: 200, height: 21)
        //label.backgroundColor = .black
        //label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        label.text = "X ="
        view.addSubview(label)
        
        var aMatrix: [[UITextField]] = []
        var nextY = startY
        
        var nextX = startX
        var aMatrixRow: [UITextField] = []
        
        for _ in 0..<cols {
            let textField: UITextField = UITextField()
            textField.frame = CGRect(x: nextX, y: startY, width: 50, height: 30)
            //textField.backgroundColor = .gray
            //textField.text = String(cols)
            textField.borderStyle = .roundedRect
            view.addSubview(textField)
            
            aMatrixRow.append(textField)
            
            nextX += 60
        }
        
        aMatrix.append(aMatrixRow)
        nextY += 40
        
        return (label: label, aMatrix: aMatrix)
    }
}
